package ru.otus.sgribkov.scaladeveloper.homework2

import ru.otus.sgribkov.scaladeveloper.homework2.task_collections.Auto

object test extends App {

  val dealerOne = Vector(Auto("BMW", "i3"), Auto("Mazda", "X5"))
  val dealerTwo = Seq(Auto("BMW", "i3"), Auto("Mazda", "X5"))

  val out = for {
    autoOne <- dealerOne
    autoTwo <- dealerTwo
    if autoOne == autoTwo
  } yield autoOne

}
