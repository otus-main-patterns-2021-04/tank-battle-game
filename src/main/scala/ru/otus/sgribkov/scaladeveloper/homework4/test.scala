package ru.otus.sgribkov.scaladeveloper.homework4

import ru.otus.sgribkov.scaladeveloper.homework4.homework_hkt_impllicts.tupleF

object test extends App {

  val optA: Option[Int] = Some(1)
  val optB: Option[Int] = Some(2)

  val list1 = List(1, 2, 3)
  val list2 = List(4, 5, 6)

  val a = tupleF(optA, optB)
  val b = tupleF(list1, list2)

  println(b)

}
