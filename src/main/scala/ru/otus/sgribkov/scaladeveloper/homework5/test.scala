package ru.otus.sgribkov.scaladeveloper.homework5

import ru.otus.sgribkov.scaladeveloper.homework5.Show
import ru.otus.sgribkov.scaladeveloper.homework5.Show._
import ru.otus.sgribkov.scaladeveloper.homework5.Monad
import ru.otus.sgribkov.scaladeveloper.homework5.Monad._

object test extends App {


  val list: List[Int] = List(1, 2, 3)
  val lts = list.mkString_[Int]("[", "]", ",")
  println(lts)

  val list2: List[Int] = List(1, 2, 3)
  val lm = list2.show
  println(lm)

  val set: Set[Int] = Set(1, 2, 3)
  val sm = set.show
  println(sm)

  val opt = Option(1)
  val om = opt.map(_ + 1)
  println(om)

  val i = 1
  val im = i.point[Set]
  println(im)

  val dList = List(List(1, 2, 3))
  val dlm = dList.flatten
  println(dlm)




}
