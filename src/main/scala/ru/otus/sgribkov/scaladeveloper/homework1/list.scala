package ru.otus.sgribkov.scaladeveloper.homework1

import scala.annotation.tailrec

object list {
  /**
   *
   * Реализовать односвязанный иммутабельный список List
   * Список имеет два случая:
   * Nil - пустой список
   * Cons - непустой, содердит первый элемент (голову) и хвост (оставшийся список)
   */

  sealed trait List[+T]{

    def ::[A >: T](elem: A): List[A] = List.::(elem, this)

    def mkString[A >: T](sep: String): String = {
      @tailrec
      def loop(accum: String, list: List[A]): String = {
        list match {
          case List.Nil => accum
          case List.::(head, tail) => loop(accum + sep + head.toString, tail)
        }
      }
      val list = this match {
        case List.Nil => this
        case List.::(_, tail) => tail
      }
      val acc = this match {
        case List.Nil => ""
        case List.::(head, _) => head.toString
      }
      loop(acc, list)
    }

    def reverse[A >: T]: List[A] = {
      @tailrec
      def loop(accum: List[A], list: List[A]): List[A] = {
        list match {
          case List.Nil => accum
          case List.::(head, tail) => loop(head::accum, tail)
        }
      }
      loop(List.Nil, this)
    }

    def map[A >: T](f: T => A): List[A] = {
      @tailrec
      def loop(accum: List[A], list: List[T], f: T => A): List[A] = {
        list match {
          case List.Nil => accum
          case List.::(head, tail) => loop(f(head)::accum, tail, f)
        }
      }
      val acc = List.Nil
      val list = loop(acc, this, e => e).asInstanceOf[List[T]]
      loop(acc, list, f)
    }

    def filter[A >: T](f: A => Boolean): List[A] = {
      @tailrec
      def loop(accum: List[A], list: List[A], f: A => Boolean): List[A] = {
        list match {
          case List.Nil => accum
          case List.::(head, tail) if f(head) => loop(head::accum, tail, f)
          case List.::(head, tail) if !f(head) => loop(accum, tail, f)
        }
      }
      val acc = List.Nil
      val list = loop(acc, this, _ => true)
      loop(acc, list, f)
    }
  }

  object List{
    case class ::[A](head: A, tail: List[A]) extends List[A]
    case object Nil extends List[Nothing]

    def apply[A](elems: A*): List[A] = {
      @tailrec
      def loop(accum: List[A], els: A*): List[A] = {
        val l = els.length
        if (l == 0) accum
        else loop(els(l - 1)::accum, els.take(l - 1): _*)
      }
      loop(Nil.asInstanceOf[List[A]], elems: _*)
    }
  }

  def incList(list: List[Int]): List[Int] = list.map(_ + 1)

  def shoutString(list: List[String]): List[String] = list.map(_ + "!")

  /**
   * Метод cons, добавляет элемент в голову списка, для этого метода можно воспользоваться названием `::`
   *
   */

  /**
   * Метод mkString возвращает строковое представление списка, с учетом переданного разделителя
   *
   */

  /**
   * Конструктор, позволяющий создать список из N - го числа аргументов
   * Для этого можно воспользоваться *
   *
   * Например вот этот метод принимает некую последовательность аргументов с типом Int и выводит их на печать
   * def printArgs(args: Int*) = args.foreach(println(_))
   */

  /**
   *
   * Реализовать метод reverse который позволит заменить порядок элементов в списке на противоположный
   */

  /**
   *
   * Реализовать метод map для списка который будет применять некую ф-цию к элементам данного списка
   */


  /**
   *
   * Реализовать метод filter для списка который будет фильтровать список по некому условию
   */

  /**
   *
   * Написать функцию incList котрая будет принимать список Int и возвращать список,
   * где каждый элемент будет увеличен на 1
   */


  /**
   *
   * Написать функцию shoutString котрая будет принимать список String и возвращать список,
   * где к каждому элементу будет добавлен префикс в виде '!'
   */

}
