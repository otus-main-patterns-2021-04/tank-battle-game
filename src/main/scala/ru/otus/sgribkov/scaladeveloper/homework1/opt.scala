package ru.otus.sgribkov.scaladeveloper.homework1

object opt {

  /**
   *
   * Реализовать метод printIfAny, который будет печатать значение, если оно есть
   */


  /**
   *
   * Реализовать метод zip, который будет создавать Option от пары значений из 2-х Option
   */


  /**
   *
   * Реализовать метод filter, который будет возвращать не пустой Option
   * в случае если исходный не пуст и предикат от значения = true
   */

  sealed trait Option[+T]{

    def isEmpty: Boolean = this match {
      case Option.Some(v) => false
      case Option.None => true
    }

    def get: T = this match {
      case Option.Some(v) => v
      case Option.None => throw new Exception("Get on empty Option")
    }

    def map[B](f: T => B): Option[B] = this match {
      case Option.Some(v) => Option.Some(f(v))
      case Option.None => Option.None
    }

    def flatMap[B](f: T => Option[B]): Option[B] = this match {
      case Option.Some(v) => f(v)
      case Option.None => Option.None
    }

    def printIfAny(): Unit = this match {
      case Option.Some(v) => println(v)
      case Option.None => ()
    }

    def zip[U](v2: Option[U]): Option[(T, U)] = {
      for {
        v1Out <- this
        v2Out  <- v2
      } yield (v1Out, v2Out)
    }

    def filter(f: T => Boolean): Option[T] = this match {
      case Option.Some(v) => if (f(v)) Option.Some(v) else Option.None
      case Option.None => Option.None
    }
  }

  object Option{
    case class Some[T](v: T) extends Option[T]
    case object None extends Option[Nothing]

    def apply[T](v: T): Option[T] = Some(v)
  }

}
