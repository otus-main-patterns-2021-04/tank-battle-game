package ru.otus.sgribkov.tankbattle.server.objects

case class MacroCommand(cmds: List[Command]) extends Command {
  def execute: Unit = {
    this.cmds.foreach(_.execute)
  }
}
