package ru.otus.sgribkov.tankbattle.server.objects

case class RotableAdapter(obj: UObject) extends IRotable {

  def getDirection: Int = {
    this.obj.props("Direction").asInstanceOf[Int]
  }

  def setDirection(direction: Int): Unit = {
    this.obj.props.update("Direction", direction)
  }

  def getAngularVelocity: Int = {
    this.obj.props("AngularVelocity").asInstanceOf[Int]
  }

  def getMaxDirections: Int = {
    this.obj.props("MaxDirections").asInstanceOf[Int]
  }
}
