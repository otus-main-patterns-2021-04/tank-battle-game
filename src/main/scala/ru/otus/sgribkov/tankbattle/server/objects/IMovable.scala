package ru.otus.sgribkov.tankbattle.server.objects

trait IMovable {
  def getPosition: Vector
  def setPosition(position: Vector)
  def getVelocity: Vector
}
