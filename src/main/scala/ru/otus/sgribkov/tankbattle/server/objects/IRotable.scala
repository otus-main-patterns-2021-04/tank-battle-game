package ru.otus.sgribkov.tankbattle.server.objects

trait IRotable {
  def setDirection(direction: Int): Unit
  def getDirection: Int
  def getAngularVelocity: Int
  def getMaxDirections: Int
}
