package ru.otus.sgribkov.tankbattle.server.objects

trait Command {
  def execute: Unit
}
