package ru.otus.sgribkov.tankbattle.server.objects

object test extends App {

  class Tank() extends UObject

  val t = new Tank
  t.setProp("Direction", 0)
  t.setProp("AngularVelocity", 10)
  t.setProp("MaxDirections", 20)

  val cmd = RotateCommand(RotableAdapter(t))
  cmd.execute
  println(t.getProp("Direction"))

}
