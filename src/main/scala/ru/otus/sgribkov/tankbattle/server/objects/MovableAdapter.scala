package ru.otus.sgribkov.tankbattle.server.objects

case class MovableAdapter(obj: UObject) extends IMovable {

  def getPosition: Vector = {
    this.obj.props("Position").asInstanceOf[Vector]
  }

  def setPosition(position: Vector): Unit = {
    this.obj.props.update("Position", position)
  }

  def getVelocity: Vector = {
    this.obj.props("Velocity").asInstanceOf[Vector]
  }
}
