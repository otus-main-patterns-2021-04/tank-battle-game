package ru.otus.sgribkov.tankbattle.server.objects

case class RotateCommand(rotable: IRotable) extends Command {
  def execute: Unit = {
    this.rotable.setDirection(
      (this.rotable.getDirection + this.rotable.getAngularVelocity) % this.rotable.getMaxDirections
    )
  }
}
