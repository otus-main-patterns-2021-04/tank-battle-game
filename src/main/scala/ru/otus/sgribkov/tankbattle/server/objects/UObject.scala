package ru.otus.sgribkov.tankbattle.server.objects
import scala.collection.mutable

trait UObject {

  var props: mutable.Map[String, Any] = mutable.Map.empty[String, Any]

  def setProp(key: String, value: Any): Unit = {
    this.props(key) = value
  }

  def getProp(key: String): Any = {
    this.props.get(key)
  }
}
