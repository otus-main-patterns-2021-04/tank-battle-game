package ru.otus.sgribkov.tankbattle.server.objects

case class Vector(coords:  List[Int]) {

  def + (that: Vector): Vector = {
    val newCoords = this.coords.zip(that.coords).map(x => x._1 + x._2)
    Vector(newCoords)
  }
}
