package ru.otus.sgribkov.tankbattle.server.objects

case class MoveCommand(movable: IMovable) extends Command {

  def execute: Unit = {
    this.movable.setPosition(this.movable.getPosition + this.movable.getVelocity)
  }
}
