package ru.otus.sgribkov.tankbattle.server.objects

import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite


class CommandsTest extends AnyFunSuite with MockFactory {

  test("MoveCommandTest") {

    val movable = mock[IMovable]
    val command = MoveCommand(movable)

    (movable.getPosition _).expects().returning(Vector(List(12, 5))).once()
    (movable.getVelocity _).expects().returning(Vector(List(-7, 3))).once()
    (movable.setPosition _).expects(Vector(List(5, 8))).returning().once()

    command.execute
  }

  test("RotateCommandTest") {

    val rotable = mock[IRotable]
    val command = RotateCommand(rotable)

    (rotable.getDirection _).expects().returning(180).once()
    (rotable.getAngularVelocity _).expects().returning(190).once()
    (rotable.getMaxDirections _).expects().returning(360).once()
    (rotable.setDirection _).expects(10).returning().once()

    command.execute
  }

}
